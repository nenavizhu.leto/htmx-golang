package main

import (
	"html/template"
	"htmx/pkg/pages"
	"htmx/pkg/database"
	"io"
	"log"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type TemplateRenderer struct {
	templates *template.Template
}

func (t* TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func main() {

	tmpls, err := template.New("").ParseGlob("public/views/*.html")

	if err != nil {
		log.Fatalf("couldn't initialize templates: %v", err)
	}

	err = database.InitContacts()
	if err != nil {
		log.Fatalf("couldn't initialize contacts db: %v", err)
	}

	// Mock data
	// database.ContactsDb.Insert(database.Contact{ Id: 0, Firstname: "Joe", Lastname: "Blow", Email: "joe@blow.com"})

	e := echo.New()
	e.Renderer = &TemplateRenderer {
		templates: tmpls,
	}

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Static("/dist", "dist")
	e.Static("/css", "css")

	e.GET("/", pages.Index)

	e.GET("/clickToEdit", pages.ClickToEdit)
	e.GET("/clickToEdit/contact/:id", pages.ClickToEdit_GetById)
	e.PUT("/clickToEdit/contact/:id", pages.ClickToEdit_PutById)
	e.GET("/clickToEdit/contact/:id/edit", pages.ClickToEdit_Edit)
	
	e.GET("/home", pages.Home)
	e.GET("/about", pages.About)
	e.POST("/about/click", pages.AboutButtonClick)

	e.Logger.Fatal(e.Start(":3000"))
}
