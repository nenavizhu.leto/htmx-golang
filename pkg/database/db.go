package database

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

type Contacts struct {
	Db *sql.DB
	insert, retrieve, update, list *sql.Stmt
}

var ContactsDb *Contacts


// Just for simplistic of use
func InitContacts() error {
	db, err := sql.Open("sqlite3", "contacts.db")
	if err != nil {
		return err
	}

	if _, err := db.Exec(`CREATE TABLE IF NOT EXISTS contacts (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		firstname TEXT NOT NULL,
		lastname TEXT NOT NULL,
		email TEXT NOT NULL
	)`); err != nil {
		return err
	}


	insert, err := db.Prepare("INSERT INTO contacts VALUES(NULL, ?, ?, ?);")
	if err != nil {
		return err
	}

	retrieve, err := db.Prepare("SELECT id, firstname, lastname, email FROM contacts WHERE id=?")
	if err != nil {
		return err
	}

	update, err := db.Prepare("UPDATE contacts SET firstname = ?, lastname = ?, email = ? WHERE id = ?")
	if err != nil {
		return err
	}

	list, err := db.Prepare("SELECT * FROM contacts WHERE id > ? ORDER BY id DESC LIMIT 100")
	if err != nil {
		return err
	}

	ContactsDb = &Contacts{
		Db: db,
		insert: insert,
		retrieve: retrieve,
		update: update,
		list: list,
	}
	
	return nil

}

func NewContacts() (*Contacts, error) {
	db, err := sql.Open("sqlite3", "contacts.db")
	if err != nil {
		return nil, err
	}

	if _, err := db.Exec(`CREATE TABLE IF NOT EXISTS contacts (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		firstname TEXT NOT NULL,
		lastname TEXT NOT NULL,
		email TEXT NOT NULL
	)`); err != nil {
		return nil, err
	}

	insert, err := db.Prepare("INSERT INTO contacts VALUES(NULL, ?, ?, ?);")
	if err != nil {
		return nil, err
	}

	retrieve, err := db.Prepare("SELECT id, firstname, lastname, email FROM contacts WHERE id=?")
	if err != nil {
		return nil, err
	}

	return &Contacts{
		Db: db,
		insert: insert,
		retrieve: retrieve,
	}, nil

}

func (c *Contacts) Insert(contact Contact) (int, error) {
	res, err := c.insert.Exec(contact.Firstname, contact.Lastname, contact.Email)
	if err != nil {
		return 0, err
	}

	var id int64
	if id, err = res.LastInsertId(); err != nil {
		return 0, err
	}

	return int(id), nil
}

func (c *Contacts) Retrieve(id int) (Contact, error) {
	log.Printf("Getting %d", id)
	row := c.retrieve.QueryRow(id)

	contact := Contact{}
	var err error

	if err = row.Scan(&contact.Id, &contact.Firstname, &contact.Lastname, &contact.Email); err == sql.ErrNoRows {
		log.Printf("Id not found")
		return Contact{}, err
	}

	return contact, err
}

func (c *Contacts) List(offset int) ([]Contact, error) {
	rows, err := c.list.Query(offset)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	data := []Contact{}
	for rows.Next() {
		i := Contact{}
		err = rows.Scan(&i.Id, &i.Firstname, &i.Lastname, &i.Email)
		if err != nil {
			return nil, err
		}
		data = append(data, i)
	}

	return data, nil
}

func (c *Contacts) Update(id int, contact *Contact) (int, error) {
	res, err := c.update.Exec(contact.Firstname, contact.Lastname, contact.Email, id)
	if err != nil {
		return 0, err
	}

	rows_affected, err := res.RowsAffected(); if 
	err != nil {
		return 0, err
	}

	return int(rows_affected), nil
}
