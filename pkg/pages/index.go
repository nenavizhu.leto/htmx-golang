package pages

import (
	"github.com/labstack/echo/v4"
)

func Index(c echo.Context) error {
	return c.Render(200, "index.html", nil)
}

func About(c echo.Context) error {
	return c.Render(200, "about", nil)
}

func AboutButtonClick(c echo.Context) error {
	return c.String(200, "VIPERR")
}

func Home(c echo.Context) error {
	return c.Render(200, "home", nil)
}
