package pages

import (
	"htmx/pkg/database"
	"strconv"

	"github.com/labstack/echo/v4"
)

func ClickToEdit(c echo.Context) error {
	contacts, err := database.ContactsDb.List(0)
	if err != nil {
		return ClickToEdit_404(c, err)
	}
	return c.Render(200, "clickToEdit", ClickToEditPage{
		Contacts: contacts,
	})
}

type ClickToEditPage struct {
	Contacts []database.Contact
	Contact database.Contact
	Error string
}

func ClickToEdit_404(c echo.Context, err error) error {
	return c.Render(404, "clickToEdit_404", ClickToEditPage{
		Contact: database.Contact{},
		Error: err.Error(),
	})
}

func ClickToEdit_GetById(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id")) 
	if err != nil {
		// Create separate func for this
		return c.String(401, "Failed to parse contact id")
	}
	
	contact, err := database.ContactsDb.Retrieve(id)
	if err != nil {
		return ClickToEdit_404(c, err)
	}

	return c.Render(200, "clickToEdit_contact", ClickToEditPage{
		Contact: contact,
	})
}

func ClickToEdit_Edit(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id")) 
	if err != nil {
		// Create separate func for this
		return c.String(401, "Failed to parse contact id")
	}
	contact, err := database.ContactsDb.Retrieve(id)
	if err != nil {
		return ClickToEdit_404(c, err)
	}

	return c.Render(200, "clickToEdit_edit", ClickToEditPage{
		Contact: contact,
	})
}

func ClickToEdit_PutById(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id")) 
	if err != nil {
		// Create separate func for this
		return c.String(401, "Failed to parse contact id")
	}
	firstname := c.FormValue("firstname")
	lastname := c.FormValue("lastname")
	email := c.FormValue("email")

	contact := database.Contact{
		Id: id,
		Firstname: firstname,
		Lastname: lastname,
		Email: email,
	}

	database.ContactsDb.Update(id, &contact)

	return c.Render(201, "clickToEdit_edit_success", ClickToEditPage{
		Contact: contact,
	})
}
